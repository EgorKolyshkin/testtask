//
//  APIClient.swift
//  TestTask
//
//  Created by Егор on 16/10/2018.
//  Copyright © 2018 Егор. All rights reserved.
//

import Foundation

class APIClient: MovieDataSource {
    
    // function ssendRequest отправляет запрос к api, через completion handler передает полученные данные
    
    func sendRequest<T: Decodable & MovieDataSource>(by parameters: [String: String], type: T.Type, completion: @escaping (T) -> ()) {
        
        let url = makeURLFromParameters(parameters)
        
        URLSession.shared.dataTask(with: url) { (data,response,error) in
            guard let jsonData = data else { return }
            do {
                let decoder = JSONDecoder()
                
                let searchResult: T = try decoder.decode(T.self, from: jsonData)
                
                completion(searchResult)
            } catch let error {
                print(error)
            }
            }.resume()
    }
    
    private func makeURLFromParameters(_ parameters: [String: String]) -> URL {
        
        var components = URLComponents()
        components.scheme = Constants.IMDB.APIScheme
        components.host = Constants.IMDB.APIHost
        components.path = Constants.IMDB.APIPath
        
        components.queryItems = parameters.map {
            URLQueryItem(name: String($0), value: String($1))
        }
        
        return components.url!
    }
}
