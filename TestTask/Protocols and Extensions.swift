//
//  Protocols and Extensions.swift
//  TestTask
//
//  Created by Егор on 18/10/2018.
//  Copyright © 2018 Егор. All rights reserved.
//

import Foundation


protocol MovieDataSource {
    
    var search: [Search] { get }
    var poster: String { get }
    
}

extension MovieDataSource {
    
    var search: [Search] {
        get {
            return []
        }
        set {}
    }
    
    var poster: String {
        get {
            return " "
        }
        set {}
    }
    
}
