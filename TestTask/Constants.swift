//
//  Constants.swift
//  TestTask
//
//  Created by Егор on 16/10/2018.
//  Copyright © 2018 Егор. All rights reserved.
//

import Foundation

struct Constants {
    
    struct IMDB {
        static let APIScheme = "https"
        static let APIHost = "www.omdbapi.com"
        static let APIPath = "/"
    }
    
    struct APIKeys {
        static let Key = "apikey"
        static let Search = "s"
        static let Plot = "plot"
        static let ID = "i"
    }
    
    struct APIValues {
        static let Key = "7e67d883"
        static let FullPlot = "full"
    }
    
}
