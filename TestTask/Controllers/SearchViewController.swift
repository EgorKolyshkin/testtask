//
//  SearchViewController.swift
//  TestTask
//
//  Created by Егор on 16/10/2018.
//  Copyright © 2018 Егор. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    // function search отпровляет запрос к api по нажатию кнопки
    
    @IBAction func search(_ sender: UIButton) {
        
        apiClient.sendRequest(by:
            [
                Constants.APIKeys.Key : Constants.APIValues.Key,
                Constants.APIKeys.Search : searchTextField.text!
        ], type: Model.self) { [weak self] model in
            let movies = model.search
            self?.movies = movies
        }
    }
    
    @IBOutlet weak var searchTextField: UITextField!
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    }
    
    
    
    private var apiClient = APIClient()
    
    private var movies: [Search] = [] {
        didSet {
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    
    private func configureUI() {
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    
    // Mark: - table view Delagete & Data Source
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return movies.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "MovieCell") else { return UITableViewCell() }
        
        cell.textLabel?.text = movies[indexPath.row].title
        
        return cell
    }
    
    
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destination = segue.destination
        if let indexPath = tableView.indexPathForSelectedRow{
            if let dmvc = destination as? DetailedMovieViewController {
                apiClient.sendRequest(by:
                    [
                        Constants.APIKeys.Key : Constants.APIValues.Key,
                        Constants.APIKeys.ID : movies[indexPath.row].id
                        
                ], type: DetailedMovieInfo.self)
                { movieInfo in dmvc.detailedInfo = movieInfo }
            }
        }
    }
    
}
