//
//  DetailedMovieViewController.swift
//  TestTask
//
//  Created by Егор on 17/10/2018.
//  Copyright © 2018 Егор. All rights reserved.
//

import UIKit

class DetailedMovieViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // public var detailedInfo экземпляр модели DetailedMovieInfo устанавливается в prepare(for segue: ... в SearchViewController 
    
    var detailedInfo: DetailedMovieInfo! {
        
        didSet {
            
            initFields(posterUrl: detailedInfo.poster, title: detailedInfo.title, country: detailedInfo.country, director: detailedInfo.director, actors: detailedInfo.actors, description: detailedInfo.plot, ratings: detailedInfo.ratings)
            
        }
    }
    
    @IBOutlet weak var posterImage: UIImageView!
    
    @IBOutlet weak var filmTitle: UILabel!
    
    @IBOutlet weak var filmCountry: UILabel!
    
    @IBOutlet weak var filmDirector: UILabel!
    
    @IBOutlet weak var filmActors: UILabel!
    
    @IBOutlet weak var filmDescription: UILabel!
    
    @IBOutlet weak var filmRatings: UILabel!
    
    // function fetchImage асинхронно загружает фото по url и устанавливает в качестве posterImage.image
    
    private func fetchImage(with url: URL) {
        
        DispatchQueue.global(qos: .userInitiated).async {
            
            let data = try? Data(contentsOf: url)
            
            DispatchQueue.main.async {
                if let imageData = data {
                    self.posterImage.image = UIImage(data: imageData)
                }
            }
        }
        
    }
    
    
    // function initFields инициализирует все UI компоненты view
    
    private func initFields(posterUrl: String, title: String, country: String, director: String, actors: String, description: String, ratings: [Rating]) {
        
        if let url = URL(string: posterUrl) {
            fetchImage(with: url)
        }
        var ratingsString = ""
        DispatchQueue.main.async {
            self.filmTitle.text = "Title: " + title
            self.filmCountry.text = "Country: " + country
            self.filmDirector.text = "Director: " + director
            self.filmActors.text = "Actors: " + actors
            self.filmDescription.text = "Description: " + description
            ratings.forEach { rating in
                ratingsString = ratingsString + "Rated by " + rating.source + " on " + rating.value + " "
            }
            self.filmRatings.text = ratingsString
        }
    }
}
