//
//  Model.swift
//  TestTask
//
//  Created by Егор on 16/10/2018.
//  Copyright © 2018 Егор. All rights reserved.
//

import Foundation

//Mark: - Model for search results

struct Model: Decodable, MovieDataSource {
    let search: [Search]
    let totalResults: String
    
    private enum CodingKeys: String, CodingKey {
        case search = "Search"
        case totalResults
    }
}

struct Search: Decodable {
    let title: String
    let id: String
    
    private enum CodingKeys: String, CodingKey {
        case title = "Title"
        case id = "imdbID"
    }
}

//Mark: - Model for detailed information about single movie

struct DetailedMovieInfo: Decodable, MovieDataSource {
    let poster: String
    let title: String
    let country: String
    let director: String
    let actors: String
    let plot: String
    let ratings: [Rating]
    
    private enum CodingKeys: String, CodingKey  {
        case poster = "Poster"
        case title = "Title"
        case country = "Country"
        case director = "Director"
        case actors = "Actors"
        case plot = "Plot"
        case ratings = "Ratings"
    }
}


struct Rating: Decodable {
    let source: String
    let value: String
    
    private enum CodingKeys: String, CodingKey {
        case source = "Source"
        case value = "Value"
    }
}
